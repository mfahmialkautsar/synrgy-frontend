import {typewrite, stopTypewrite} from './typewrite.js';
import {sleep} from './util.js';
function greet() {
  const greetOuter = $('.greet-outer');
  const greet1 = $('.greet-1');
  const greet1Inner = $('.greet-1-inner');
  const greet2 = $('.greet-2');
  greet1.addClass('greet-1--animate');
  greet1Inner.addClass('greet-1-inner--animate');
  greet2.addClass('greet-2--animate');
  greetOuter.removeClass('d-none');
  greet1.on('animationstart', () => {
    stopTypewrite();
  });
  greet2.on('animationend', () => {
    sleep(1000);
    typewrite();
  });
}
export default greet;
